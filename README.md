# XRP Action

Spike project for Angular 8.  
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.21.


## Deploy to production

Assume we have a Linux VPS with Docker installed and we want to run Angualr on top of nginx.

Steps:
- In CircleCI or GitLabCI:
  - Build Angular 
  - Prepare a distribution folder (minification and optimization)
  - Create a Docker image based on nginx  
  - Copy the Angular builded folder
  - Set sone configuration with secret values
  - Publish the image on a private Image repository
  - Connet an SSH command on the VPS
  - Copy the new deploy script 
  - Execute the deploy script, that will execute this steps
    - Pull the newly created image 
    - Create and run a new container with the new image
    - Switch the containers, the new one will respond to the requests on port 80
    - smoke test
    - Delete the old container
    - Delete the old image 
    - Return OK

## Development

Run `ng serve --open` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.  
``--open`` will open the website ion the default browser.



## Production 

### Server
- lite-server
- nginx
- Firebase Hosting: https://firebase.google.com/docs/hosting
- Zeit: https://zeit.co/home

### Deploy on production

``ng build --watch``



## Angular: Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Angular: Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Angular: Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Angular: Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Angular: Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
