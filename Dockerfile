# build Angular
#FROM node:12.2.0 as builder
FROM registry.gitlab.com/alessandro.piccione/docker-images/angular as builder

# passed with --build-arg "0.1" is the default
ARG VERSION=0.1

WORKDIR /usr/src/app

RUN echo "User name= $u"

#USER root

COPY ./Angular_website/ .

RUN echo "version: $VERSION"
RUN sed -i "s/{version}/$VERSION/g" ./src/index.html

#RUN sudo chown -R $USER /usr/local/lib/node_modules
RUN chown -R $USER:$USER /usr/local/lib/node_modules \
    && chown -R $USER:$USER /usr/local/bin

RUN npm install

RUN chown -R $USER:$USER .
RUN ng build --prod


#COPY ./Angular\ website/src /app
# fucking Docker ADD and COPY fail when path contains space
#COPY ./Angular_website/src/index.html .
#COPY ./Angular_website/dist/xrp-action .


# pepare NGINX
FROM nginx:1.17

# path to web site: "Angular website/src/index.html"

# COPY ./index.html /usr/share/nginx/html
COPY --from=builder /usr/src/app/dist/xrp-action /usr/share/nginx/html


#EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"] 