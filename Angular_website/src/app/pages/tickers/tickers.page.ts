import { Component, ViewChild } from '@angular/core'
import {ChartDataSets, ChartOptions, ChartColor} from "chart.js"
import {Color, BaseChartDirective, Label} from "ng2-charts"

import { BasePage } from "src/app/pages/BasePage" 

@Component({
    selector: "tickers-page",
    templateUrl: "./tickers.page.html"
})
export class TickersPage extends BasePage {

    public chartData:ChartDataSets[] = undefined
    public chartLabels:Label[] = undefined
    public chartOptions:ChartOptions = undefined
    public chartColors:Color[] = undefined
 
    @ViewChild(BaseChartDirective, {static:true}) chart:BaseChartDirective

    public loadChart() {

        this.chartData = [
            {data: [0.17739, 0.17739, 0.17059, 0.17014, 0.17190, 0.17078, 0.16967], label:"Bitstamp - Ask"},    
            {data: [0.17680, 0.17680, 0.17003, 0.16967, 0.17176, 0.17017, 0.16922], label:"Bitstamp - Bid"},
                
            {data: [0.17721, 0.17721, 0.16981, 0.16979, 0.17161, 0.17053, 0.16958], label:"Kraken - Ask"},
            {data: [0.17707, 0.17707, 0.16972, 0.16976, 0.17157, 0.17037, 0.16943], label:"Kraken - Bid"},
               
            {data: [0.17840, 0.17840, 0.17060, 0.17060, 0.17250, 0.17140, 0.17040], label:"CEX IO - Ask"},
            {data: [0.17760, 0.17760, 0.17020, 0.17020, 0.17220, 0.17080, 0.17000], label:"CEX IO - Bid"},                
        ]

        this.chartLabels = ["12/12 16:00", "12/12 16:00", "26/12 15:15", "26/12 15:45", "26/12 20:15", "27/12 00:15", "27/12 23:00"]

        this.chartOptions = {
            responsive: true,
            scales: { 
                xAxes: [{}],
                yAxes: [
                    {id: "yAxis-1",
                    position: "left"}
                ]
            }
        }

        let transparent = "rgba(0,0,0 ,0)"
        let white = "#fff"
        let bitstampColor = "rgba(21, 146, 73, 1)"
        let krakenColor = "rgba(87, 65, 217, 1)"  //"rgba(126, 87, 194, 1)"
        let cexioColor = "rgba(0, 189, 202, 1)"
        let crex24Color: "rgba(22, 93, 104, 1)" // https://encycolorpedia.com/165d68
        
        let lineWidth = 1

        // https://htmlcolorcodes.com/color-chart/
        this.chartColors = [
            this.createColor(bitstampColor),
            this.createColor(bitstampColor),
            this.createColor(krakenColor),
            this.createColor(krakenColor),
            this.createColor(cexioColor),
            this.createColor(cexioColor),          
        ]

        this.chart.chartType = "line"
        this.chart.update()
    }

    private createColor(baseColor:string, ask_bid:string="ask", lineWidth:number=1) {
        return { 
            borderColor:          baseColor,
            backgroundColor:      "rgba(0,0,0, 0)",              
            pointBackgroundColor: baseColor,
            pointBorderColor:     '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor:     baseColor,

            borderWidth: lineWidth,
            lineTension: 0,
            pointBorderWidth: 1   
          }
    }
}