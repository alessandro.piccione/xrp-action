import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { ChartsModule } from "ng2-charts"

import { AppComponent } from './app.component'
import { TickersPage } from "src/app/pages/tickers/tickers.page"
import { ChartExamplePage} from "src/app/pages/chart example/chart example.page"
import { from } from 'rxjs'

@NgModule({
  declarations: [
    AppComponent,
    // page
    TickersPage,
    ChartExamplePage
  ],
  imports: [
    BrowserModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
