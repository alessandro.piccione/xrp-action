# NGINX

https://www.docker.com/blog/tips-for-deploying-nginx-official-image-with-docker/  



# docker run --name mynginx1 -P -d nginx

fcd1fb01b14557c7c9d991238f2558ae2704d129cf9fb97bb4fadf673a58580d  

This command creates a container named mynginx1 based on the NGINX image and runs it in detached mode, meaning the container is started and stays running until stopped but does not listen to the command line. We will discuss later how to interact with the container.   
  
The NGINX image exposes ports 80 and 443 in the container and the -P option tells Docker to map those ports to ports on the Docker host that are randomly selected from the range between 49153 and 65535. We do this because if we create multiple NGINX containers on the same Docker host, we create a conflict on ports 80 and 443. The port mappings are dynamic and are set each time the container is started or restarted. If you want the port mappings to be static, set them manually with the -p option. The long form of the Container Id will be returned.


## Load Balancer

https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/

