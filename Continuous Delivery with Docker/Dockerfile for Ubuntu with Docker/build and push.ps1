# Build a docker image with Docker

write-host "Build Docker"

$version="0.4.2" 

docker build -t alessandropiccione/docker-publisher:$version -t alessandropiccione/docker-publisher:latest .
docker push alessandropiccione/docker-publisher:$version
docker push alessandropiccione/docker-publisher:latest

write-host "end"