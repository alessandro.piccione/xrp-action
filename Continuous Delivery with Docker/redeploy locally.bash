#!/bin/bash

# if user has no permission add it to docker group
# create docker group if not exists
# sudo groupadd docker
# sudo usermod -aG docker $user
# needs to loout to take effect, or try "su $user" to login again

image=registry.gitlab.com/alessandro.piccione/xrp-action
container_name=xrp-action

echo Redeploy

docker image pull $image
#docker images
#docker images -f label=$image does not work
# find the running container
#docker ps -q -filter ...
current=$(docker container ls -a -q --filter name=$container_name)
echo current is $current
# if exists
if [ -n "$current" ];
then
    echo old container found!
    docker container stop $container_name
    docker container rm $container_name
else
    echo cold ontainer NOT found
fi

# run the container with the new image
docker run -d -p 81:80 --name $container_name $image