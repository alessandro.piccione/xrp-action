# Delivery with Docker

GitLab Docker build Docker (Docker in Docker).
https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

Set the image url in a variable to simplify coding. 
bash: ``img=registry.gitlab.com/alessandro.piccione/xrp-action`` 
powershell: ``$img = ...``


## Create a nginx container
```powershell 
$imgid = docker container create --name nginx_1 --cidfile C:/Logs/docker_nginx_container.txt  nginx:1.17
```

## Find a container

```powershell 
$a = docker container ls -a --filter name=nginx_1
```

## Update a container
```powershell
docker container update 
```

## Run the image

> Important!
> https://docs.docker.com/engine/reference/run/#detached--d

``$ docker run -d -p 80:80 my_image nginx -g 'daemon off;'``

```powershell 
$img_id = docker run -d -p 81:80 --name xrpaction registry.gitlab.com/alessandro.piccione/xrp-action
Write-Host $img_id
```

```sh
img_id= docker run -d -p 81:80 --name xrpaction registry.gitlab.com/alessandro.piccione/xrp-action
echo $img_id
```


## Update the container with nw image

echo update image with latest
docker pull $img
echo stop cntainer with image

## Stop and remove a running image

docker images -f label=registry.gitlab.com/alessandro.piccione/xrp-action

.. todo: filter does not wotk !
```sh
docker images -f name="" rm < docker images 
```

## Run the official nginx image

``docker run -d -p 81:80 --name nginx_test:v2 nginx``


## Publish a container in GitLab

``docker login registry.gitlab.com``
``docker build -t registry.gitlab.com/alessandro.piccione/xrp-action .``
``docker push registry.gitlab.com/alessandro.piccione/xrp-action``

Build with tags:
``docker build -t registry.gitlab.com/alessandro.piccione/xrp-action:0.1 -t registry.gitlab.com/alessandro.piccione/xrp-action:latest .``


[X] create an image
  [-] Dockerfile based on nginx
  [X] Build local (``docker build -t nginx_test:2 "Continuous Delivery with Docker"``)
  [X] Test it locally
  - ``$(docker container rmi docker nginx_test)``
  - ``docker container rm nginx_test``
  - ``docker run -d -P --name nginx_test nginx_test:2``)
  - ``docker run -d -p 82:80 --name nginx_test nginx_test:2``)
  - ``open http://localhost:80``
  [ ] Publish
  [ ] Add Angular in it
  [ ] Test locally
[X] publish the image
[ ] use local docker to run the image

## Create an image

> docker image build [OPTIONS] PATH | URL | -

``` terminal
docker build -t nginx_test:v1 -t nginx_test:latest
```


## Publish the image
```bash
docker image publish
```

